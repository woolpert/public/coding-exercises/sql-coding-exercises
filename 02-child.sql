/* FYI - SQLite does not enforce FKs without this. */
PRAGMA foreign_keys = ON;
.headers on
.mode column

/* A table for parcels */
CREATE TABLE parcel (
  id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  detail varchar(40)
);

/*
  A table for tax assessments. Each parcel can have
  many tax assessments
*/
CREATE TABLE assessment (
  id integer PRIMARY KEY AUTOINCREMENT NOT NULL,
  a_year integer,
  a_value decimal(10,2),
  par_id integer not null
  /*
    TODO - adjust this table schema such that:
    
    1. The INSERT statement FAILS for an `assessment` record that
       references a non-existent parcel record 
    2. DELETE-ing a parcel also deletes associated `assessment` records.
  */
);


/* Add a couple of parcels */
INSERT INTO parcel(detail) VALUES('Town hall');
INSERT INTO parcel(detail) VALUES('Coffee shop');

/* Add a couple of assessments for each parcel */
INSERT INTO assessment(a_year, a_value, par_id) VALUES(2019, 100000.00, 1);
INSERT INTO assessment(a_year, a_value, par_id) VALUES(2020, 114001.00, 1);
INSERT INTO assessment(a_year, a_value, par_id) VALUES(2019, 250000.00, 2);
INSERT INTO assessment(a_year, a_value, par_id) VALUES(2020, 266000.00, 2);

/*
  TODO - Make this INSERT statement fail.
  Parcel ID = 3 does not exist.
*/
INSERT INTO assessment(a_year, a_value, par_id) VALUES(2020, 266000.00, 3);


DELETE FROM parcel where id=1;
/*
  TODO - Make the SELECT statement print 2 instead of 4.
  This means that assessments for parcel ID=1 should have
  been DELETEd by the previous statement.
*/
SELECT COUNT(*) as assessments FROM assessment;

DROP TABLE assessment;
DROP TABLE parcel;