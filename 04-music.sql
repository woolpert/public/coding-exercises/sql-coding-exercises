/*
  TODO - print the artist name and album count

  ArtistName  AlbumCount
  ----------  ----------
  Lost        3         
  Creedence   2         
  The Office  2  

  ONLY those artists who have released:
  
    - at least 2 albums
    - each having at least 20 tracks on them.

    Tip: the .tables and .schema [table] commands are handy!
*/
.open sample.db
.headers on
.mode column
