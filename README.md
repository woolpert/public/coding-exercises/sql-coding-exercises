# Exercise Your SQL Knowledge

This sample uses a small SQLite database to explore the SQL language and RDBMS basic principles.

## Get started

Look at the contents of `sample.sql`.  It performs two inner joins across three tables to show a list of tracks, albums, and artists.

Use the `Shell` window and _type_ the following:

> TIP: DO NOT cut and paste these lines. Type them instead.
> The `$` is a prompt and is **not** something you type.

```sqlite
$ sqlite3
sqlite> .open sample.db
sqlite> .read sample.sql
sqlite> .quit
```

### HELP!

If you find yourself in a weird state, you may be half-way through typing a SQL statement and not realize it.
For example, see how SQLite keeps asking for more input here?

```sqlite
sqlite> sqlite
   ...> .read sample.sql
   ...>
```

SQLite thinks you're typing a multi-line SQL statement.
Just type a semicolon `;` to finish the statement and you should be go to go.


## // TODO - Your SQL Exercises

There are several numbered exercises contained in SQL scripts. The script file names start with a number and end with `sql`, like `01....sql` and `02.....sql`. 

Edit those _incomplete_ SQL scripts so that you get the output or behavior required.

> TIP: there's handy guidance and subtle clues in the TODO text.
> Read the SQL files carefully.


## Handy SQLite commands

Now you are in a SQLite session. Use [various metadata commands](https://www.sqlitetutorial.net/sqlite-commands/) to discover the structure of the database.
For example:

- See a list of commands: `.help`
- Open an existing SQLite database file: `.open your.db`
- List the tables: `.tables`
- Look at the schema for one table: `.schema tracks`
- List indexes: `.indexes`
- List indexes in a specific table: `.indexes tracks`
- Get out of the current session: `.quit`
