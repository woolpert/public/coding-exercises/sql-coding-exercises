.headers on
.mode colum

CREATE TABLE assessment (
  id integer primary key,
  assessed_area integer not null,
  total_area integer not null
  /*
    TODO - adjust the table schema so that
    1. The FIRST insert statement 
    below fails.
    1. The SECOND insert statment fails.
  */
);

/*
  DON'T break this!
*/
INSERT INTO assessment(id, assessed_area, total_area)
VALUES (1, 210, 324);

/*
  TODO - Ensure that this INSERT statement fails
  because you *check* that Assessed Area **must** is less
  than-or-equal-to the Total Area.
*
INSERT INTO assessment(id, assessed_area, total_area)
VALUES (2, 400, 324);

DROP TABLE assessment;