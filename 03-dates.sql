.mode columns
.headers on

CREATE TABLE CAMA
(
   lrsn INT,
   /*
     SQLite does not have a native DATE type.
     Instead use TEXT with native date functions.
     But MIN and MAX functions work as you probably expect:

     https://www.sqlite.org/lang_datefunc.html
   */
   appraisal_date TEXT,
   collection_date TEXT,
   data_collector VARCHAR(100)
);

INSERT INTO CAMA(lrsn, appraisal_date, collection_date, data_collector)
VALUES (123, '2008-01-01', '2007-07-01', 'bob');

INSERT INTO CAMA(lrsn, appraisal_date, collection_date, data_collector)
VALUES (123, '2010-01-01', '2009-10-06', 'alice');

INSERT INTO CAMA(lrsn, appraisal_date, collection_date, data_collector)
VALUES (123, '2011-01-01', '2009-10-06', 'alice');

INSERT INTO CAMA(lrsn, appraisal_date, collection_date, data_collector)
VALUES (456, '2009-01-01', '2008-07-01', 'alice');

INSERT INTO CAMA(lrsn, appraisal_date, collection_date, data_collector)
VALUES (678, '2008-01-01', '2007-08-01', 'mary');

INSERT INTO CAMA(lrsn, appraisal_date, collection_date, data_collector)
VALUES (678, '2017-01-01', '2016-08-01', 'kate');

INSERT INTO CAMA(lrsn, appraisal_date, collection_date, data_collector)
VALUES (678, '2020-01-01', '2017-08-01', 'kate');


/*
  TODO - write a query that shows:

  1. One row per-LSRN
  2. A column with the earliest appraisal_date for that LSRN
  3. A column with the most recent appraisal_date for that LSRN
   
  lrsn        earliest    latest    
  ----------  ----------  ----------
  123         2008-01-01  2011-01-01
  456         2009-01-01  2009-01-01
  678         2008-01-01  2020-01-01
*/


DROP TABLE CAMA;